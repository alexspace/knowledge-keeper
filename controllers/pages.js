'use strict';

exports.index = (res, req) => {
    res.render('index', {
        title: 'Main page',
        message: 'Hi hi hi!',
        meta: {
            description: 'My cool app!',
            charset: 'utf-8'
        }
    });
};

exports.error404 = (res, req) => {
    res.sendStatus(404);
};
