'use strict';
let pages = require('../controllers/pages');
let books = require('../controllers/books');

module.exports = function(app) {
    app.get('/', pages.index);
    app.get('/books', books.list);
    app.get('/books/:name', books.item);
    app.get('*', pages.error404);
}
