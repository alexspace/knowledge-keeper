'use strict';

// let http = require('http');
// let url = require('url');
// let fs = require('fs');
// let Router = require('../router/router');

let express = require('express');
let app = express();

app.set('view engine', 'hbs');
require('../routes/router')(app);

app.listen(8080);
